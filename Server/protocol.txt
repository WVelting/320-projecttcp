Connect4 Protocol (C4P) v1.0
==================================

What game state needs to be stored on the server?
 - state of the board
 - whose turn it is
 - if someone has one


Packets from server
====================

JOIN(response) - When the server receives a JOIN packet, it must decide
whether or not the client is allowed to join.

    0   L   Datatype    Desc
    ========================================================
    0   4   ascii       packet type: "JOIN"
    4   1   uint8       join response (see below)
    

    (accepted:)
    1: player "X"
    2: player "O"
    3: spectator
    (denied:)
    4: username too short
    5: username too long
    6: username has invalid characters
    7: username already exists
    8: username not allowed (profanity)
    9: game is full

UPDT = The state of the game

    0   L   Datatype    Desc
    ========================================================
    0   4   ascii       packet type: "UPDT"
    4   1   uint8       whose turn (1 or 2)
    5   1   uint8       who's won? (see below)
    6   9   uint8 x9    the values in spots 1-9 (00000000 = empty, 00000001 = X, 00000010 = O)

    who's won
    ===========================================================
    0: playing the game ... (no winner)
    1: player "X" has won
    2: player "0" has won
    3: cat's game (no winner)
    4: game canceled

CHAT = When receiving a CHAT packet from a client, the server sends
this CHAT packet out to all connected, except the sender

    0   L   Datatype    Desc
    ========================================================
    0   4   ascii       packet type: "CHAT"
    4   1   uint8       length of the sender's username
    5   2   uint16      length of the message
    7   ?   ascii       sender's username
    ?   ?   ascii       chat message



Packets from client
=====================


JOIN (request) = after establishing a connection w/ the server, client
will send a JOIN packet w/ a desired username. Server will send back
a response

(11110010 10011010 11001000 00000000) = "JOIN" (00000001) = "1" (10100101) = username

    0   L   Datatype    Desc
    ========================================================
    0   4   ascii   packet type: "JOIN"
    4   1   uint8   how long the username is(255 max)
    5   ?   ascii   the desired username for the connected client


CHAT - player wants to chat

    0   L   Datatype    Desc
    ========================================================
    0   4   ascii       packet type: "CHAT"
    4   1   uint8       length of message
    5   ?   ascii       message for the chat

PLAY = This packet should be submitted when the player takes their turn
    0   L   Datatype    Desc
    ========================================================
    0   4   ascii       packet type: "PLAY"
    4   1   uint8       X (column number)
    5   1   uint8       Y (row number)