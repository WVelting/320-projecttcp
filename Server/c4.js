

const server = require("./server").Server;
const PacketBuilder = require("./PacketBuilder").PacketBuilder;


const Game = 
{
    whoseTurn: 1,
    whoHasWon: 0,
    board: 
    [
        [0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0]
    ],
    
    
    clientRed: null, //player 1
    clientBlue: null, //player 2
    playMove(client, x, y)
    {
        //Ignore moves after game has ended
        if(this.whoHasWon > 0) return;
        
        //Ignore everyone but whoose turn it is
        if(this.whoseTurn == 1 && client != this.clientRed) return;
        if(this.whoseTurn == 2 && client != this.clientBlue) return;
        
        
        //Ignore invalid moves
        if(x < 0) return;
        if(y< 0) return;
        if(y >= this.board.length) return;
        if(x >= this.board[y].length) return;

        

        //Ignore moves on taken spaces
        if(this.board[y][x] > 0) return;

        console.log(this.board[y][x]);
        this.board[y][x] = this.whoseTurn;
        console.log(this.board[y][x]);
        this.whoseTurn = (this.whoseTurn == 1) ? 2 : 1; //toggles the turn

        this.checkStateAndUpdate();
    },
    checkStateAndUpdate()
    {
        console.log("broadcasting...");
        //TODO: check for game over
        const packet = PacketBuilder.update(this)
        //broadcast that packet
        server.broadcastPacket(packet);
    },
    // sendNumberOfClients()
    // {
    //     console.log("sending number of clients");
    //     const packet = PacketBuilder.start(this);
    //     server.broadcastPacket(packet);
    // }

}

server.start(Game);