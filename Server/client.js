
const { PacketBuilder } = require("./PacketBuilder");




exports.Client = class Client {
    constructor (socket, server) {
        this.socket = socket;
        this.server = server;
        this.username = "";
        this.lengthOfUsername;
        this.buffer = Buffer.alloc(0);
        this.socket.on("error", (e) => this.onError(e));
        this.socket.on("close", () => this.onClose());
        this.socket.on("data", (d) => this.onData(d));
    };

    onError(errMessage){
        console.log("ERROR with client: " + errMessage);

    };

    onClose(){
        this.server.onClientDisconnect(this);

    };

    onData(data){

        this.buffer = Buffer.concat([this.buffer, data]);

        if (this.buffer.length < 4) return;

        const packetIdentifier = this.buffer.slice(0, 4).toString();

        switch(packetIdentifier) {
            case "JOIN": 
                if(this.buffer.length <5) return;

                const lengthOfUsername = this.buffer.readUInt8(4); //the 5th byte
                if(this.buffer.length<5+lengthOfUsername) return;

                const desiredUsername = this.buffer.slice(5, 5 + lengthOfUsername).toString();

                console.log("Client wants name to be " + desiredUsername);
                //check username...
                let responseType = this.server.generateResponseID(desiredUsername, this);
                this.username = desiredUsername;
                
                this.lengthOfUsername = lengthOfUsername;
                console.log(responseType);
                

                //consume the data
                this.buffer = this.buffer.slice(5 + lengthOfUsername);

                //respond
                let numOfUsers = this.server.numOfUsers;
                console.log("This many users:" + this.server.numOfUsers);
                const packet = PacketBuilder.join(responseType, numOfUsers);

                this.server.broadcastPacket(packet);

                // this.server.game.sendNumberOfClients(game);
                

                // const packet2 = PacketBuilder.update(this.server.game);
                // this.sendPacket(packet2);

                

                

                break;
                //make sure that is long enough

            case "CHAT": 
                
                console.log("Chat received");
                if(this.buffer.length<5) return;

                const lengthOfMessage = this.buffer.readUInt8(4);
                if(this.buffer.length<5+lengthOfMessage) return;
                var clientToSkip = this.server.clients.indexOf(this);
                console.log(clientToSkip);
                
                
                const message = this.buffer.slice(5, 5 + lengthOfMessage).toString();
                console.log(this.username + "wants to say: " +message);

                //consume the data
                this.buffer = this.buffer.slice(5 + lengthOfMessage);
                

                //respond
                const packet2 = PacketBuilder.chat(this.lengthOfUsername, this.username, lengthOfMessage, message);
                
                
                this.server.broadcastPacket(packet2, clientToSkip);
                console.log("Chat sent");


                break;
            case "PLAY": 
                if(this.buffer.length < 6) return;

                const x = this.buffer.readUInt8(4);
                const y = this.buffer.readUInt8(5);

                console.log("User wants to play at: " + x + " " + y);

                this.buffer = this.buffer.slice(6);
                
                //do something with that info
                this.server.game.playMove(this,x,y);
                break;
            default:
                //dont recognize the packet
                console.log("ERROR: packet identifier not recognized ("+packetIdentifier+")");
                this.buffer = Buffer.alloc(0);
                break;
            
        }

        

    };

    sendPacket(packet)
    {
        this.socket.write(packet);
    };
};