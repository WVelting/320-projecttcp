const { start } = require("repl");

exports.PacketBuilder = 
{

    join(responseID, numOfUsers)
    {
        const packet = Buffer.alloc(6);

        packet.write("JOIN", 0);
        packet.writeUInt8(responseID, 4);
        packet.writeUInt8(numOfUsers, 5);
        return packet;

    },
    chat(lengthOfUsername, username, lengthOfMessage, message)
    {
        const packet = Buffer.alloc(7 + lengthOfUsername + lengthOfMessage);

        packet.write("CHAT", 0);
        packet.writeUInt8(lengthOfUsername, 4);
        packet.writeUInt8(lengthOfMessage, 5);
        packet.write(username, 6);
        packet.write(message, 7 + lengthOfUsername);
        return packet;

    },
    update(game)
    {
        const packet = Buffer.alloc(48);
        packet.write("UPDT", 0);
        packet.writeUInt8(game.whoseTurn, 4);
        packet.writeUInt8(game.whoHasWon, 5);

        let offset = 6;
        for (let y = 0; y<game.board.length; y++)
        {
            for (let x = 0; x<game.board[y].length; x++)
            {
                packet.writeUInt8(game.board[y][x], offset);
                offset++;
            }
        }
        return packet;

    },
    // start(game)
    // {
    //     const packet = Buffer.alloc(5);
    //     packet.write("STRT", 0);
    //     packet.writeUInt8(game.numOfClients, 4);
    //     return packet;

    // }
    
}