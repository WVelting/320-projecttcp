
const Client = require ("./client.js").Client;
exports.Server = {
    port: 8080,
    clients: [],
    maxConnectedUsers: 8,
    numOfUsers: 0,
    
    start(game) {

        this.game = game;
        this.socket = require("net").createServer({}, c=>this.onClientConnect(c));
        this.socket.on("error", e=> this.onError(e));
        this.socket.listen({port: this.port}, ()=>this.onStartListen());
    },
    onClientConnect(socket){
        console.log("New connection from " + socket.localAddress);

        if(this.isServerFull())
        {   
            const packet = PacketBuilder.join(9);
            socket.end(packet);

        }
        else
        {
            const client = new Client(socket, this);
            this.clients.push(client);

        }

        //instantiate a new Client object
    },
    
    onError(e){
        console.log("ERROR with listener: " + e);
    },
    onStartListen() {
        console.log("Server is now listening on port " + this.port);
    },
    onClientDisconnect(client){
        if (this.game.clientX == client) this.game.clientX = null;
        if(this.game.clientO == client) this.game.clientO = null;
        const index = this.clients.indexOf(client);
        this.clients.splice(index,1);
    },
    isServerFull()
    {
        return(this.clients.length >= this.maxConnectedUsers);
    },
    generateResponseID(desiredUsername, client)
    {
        if(desiredUsername.length < 3) return 4;
        if(desiredUsername.length > 12) return 5;

        const regex1 = /^[a-zA-Z0-9\[\]]+$/; //literal regex in javascript
        if(!regex1.test(desiredUsername)) return 6; //invalid characters

        let isUsernameTaken = false;
        this.clients.forEach( c=>
            {
                if (c==client) return;
                if (c.username == desiredUsername) isUsernameTaken = true;
            });
        if(isUsernameTaken) return 7; //username taken

        const regex2 = /(fuck|fvck|shit|piss|damn)/i;
        if(regex2.test(desiredUsername)) return 8; //bad words

        
        
        if(!this.game.clientRed)
        {
            this.game.clientRed = client;
            this.numOfUsers++;
            console.log("There are " + this.numOfUsers + " many clients");
            console.log("You are client Red");
            
            return 1; //you are now client Red
        }
        
        if(!this.game.clientBlue)
        {
            this.game.clientBlue = client;
            this.numOfUsers++;
            console.log("There are " + this.numOfUsers + " many clients");
            console.log("You are Client Blue");
            return 2; //you are now client Blue
        }
        
        if(this.game.clientRed == client) return 1; //you are already client Red
        if(this.game.clientBlue == client) return 2; //you are already client Blue
        return 3; //you are a spectator
        
    },
    broadcastPacket(packet, clientToSkip)
    {
        this.clients.forEach( c =>
            {
                if(this.clients.indexOf(c) != clientToSkip) c.sendPacket(packet);
                else return;
            });
    }
    

};
