using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum Player
{
    Nobody,
    PlayerRED,
    PlayerBLUE
}

public class ControllerGameplay : MonoBehaviour
{

    private int columns = 7;
    private int rows = 6;
    public ButtonRB prefab; //reference to the idea of a button
    public Transform panelGameBoard;
    
    private int[,] boardData;
    private ButtonRB [,] boardUI;
    // Start is called before the first frame update
    void Start()
    {
        BuildBoardUI();
    }

    private void BuildBoardUI()
    {
        boardUI = new ButtonRB[columns, rows];

        for (int x=0; x< columns; x++)
        {
            for (int y=0; y< rows; y++)
            {
                ButtonRB bttn = Instantiate(prefab, panelGameBoard);
                //init the button
                bttn.Init(new GridPos(x, y), () => {ButtonClicked(bttn);});
                boardUI[x,y] = bttn;
            }

        }
    }

    private void ButtonClicked(ButtonRB bttn)
    {
        ControllerGameplayClient.singleton.SendPlayPacket(bttn.pos.X, bttn.pos.Y);
    }

    public void UpdateFromServer(byte gameStatus, byte whoseTurn, byte[] spaces)
    {
        print("Update from server...");

        for (int i = 0; i < spaces.Length; i ++)
        {
            byte b = spaces[i];

            int x = i % 7;
            int y = i/7;

            boardUI[x,y].SetOwner(b);
            print(b);
        }

    }
}
