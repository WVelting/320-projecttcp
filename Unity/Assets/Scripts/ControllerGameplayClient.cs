using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using TMPro;
using UnityEngine;


public class ControllerGameplayClient : MonoBehaviour
{
    public static ControllerGameplayClient singleton;
    public TMP_InputField inputHost;
    public TMP_InputField inputPort;
    public TMP_InputField inputUsername;
    public TMP_InputField ChatEntry;
    public TextMeshProUGUI readyUp;
    public TextMeshProUGUI startTheGame;
    public TextMeshProUGUI chatWindow;
    
    public GameObject PanelConnection;
    public GameObject PanelUsername;
    public GameObject PanelGameplay;
    public ControllerGameplay game;
    public byte numOfUsers;
    public enum Panel
    {
        Connection,
        Username,
        Gameplay

    }

    private Buffer buffer = Buffer.Alloc(0);
    private TcpClient socket = new TcpClient();
       



    // Start is called before the first frame update
    void Awake()
    {   
        //if already set...
        if(singleton)
        {
            //destroy
            Destroy(gameObject);
        }   
        else
        {
            singleton = this;
            DontDestroyOnLoad(gameObject);

        }

        GoToPanel(Panel.Connection);

        
        // Buffer buf = Buffer.From("Hello ");
        // Buffer otherbuff = Buffer.From("world!");
        // buf.Concat(otherbuff);
        // print(buf.ReadString());
        
    }

    public void OnButtonConnect()
    {
        //get the host from the input field
        //get the port from the input field

        string host = inputHost.text;
        UInt16.TryParse(inputPort.text, out ushort port);

        TryToConnect(host, port);
        //make it connect

    }

    public void OnButtonUsername()
    {
        string user = inputUsername.text;
        //create packet
        Buffer packet = PacketBuilder.Join(user);
        //send packet
        SendPacketToServer(packet);
        readyUp.text = "Ready!";

        
        
    }

    public void OnButtonDisconnect()
    {
        socket.Close();
        socket = new TcpClient();
        GoToPanel(Panel.Connection);
    }

    public void OnButtonStart()
    {
        if(numOfUsers >=2) GoToPanel(Panel.Gameplay);
    }

    public void InputFieldEdit(string s)
    {
        Buffer packet = PacketBuilder.Chat(s);
        SendPacketToServer(packet);
        ChatEntry.text = "";
    }

    
    public void GoToPanel(Panel panel)
    {
        if(panel == Panel.Connection)
        {
            PanelGameplay.SetActive(false);
            PanelConnection.SetActive(true);
            PanelUsername.SetActive(false);
        }
        else if(panel == Panel.Username)
        {
            PanelGameplay.SetActive(false);
            PanelConnection.SetActive(false);
            PanelUsername.SetActive(true);
        }
        else if(panel == Panel.Gameplay)
        {
            PanelGameplay.SetActive(true);
            PanelConnection.SetActive(false);
            PanelUsername.SetActive(false);
        }
    }

    async public void TryToConnect(string host, int port)
    {
        if (socket.Connected) return;

        try
        {

            print("Connecting...");
            await socket.ConnectAsync(host, port);
            print("Connected.");
            GoToPanel(Panel.Username);

            StartReceivingPackets();
            //start receiving packets
        }
        catch (Exception e)
        {
            print("FAILED TO CONNECT..." + e);
            GoToPanel(Panel.Connection);

            //TODO: display a message to the player
        }
        
    }

    public void AddMessageToChatDisplay(string user, string mess)
    {
        chatWindow.text += user + ": " + mess + "\n";
    }

    async private void StartReceivingPackets()
    {
        int maxPacketSize = 4096;

        while(socket.Connected)
        {
            byte [] data = new byte[maxPacketSize];

            try
            {
                int bytesRead = await socket.GetStream().ReadAsync(data, 0, maxPacketSize);
                buffer.Concat(data, bytesRead);
                //process our packets
                ProcessPackets();
            }

            catch(Exception e)
            {
                print("ERROR HANDLING DATA..." + e.Message);

            }

        }
    }

    private void ProcessPackets()
    {
        if(buffer.Length < 4) return;

        // PLAY1483958CHAT29813247
        // consume once packet is finished

        string identifier = buffer.ReadString(0, 4);

        print(identifier);

        switch(identifier)
        {
                        
            case "JOIN":
                if (buffer.Length < 6) return;

                byte joinResponse = buffer.ReadByte(4);
                numOfUsers = buffer.ReadByte(5);
                print(numOfUsers);

                
                if(joinResponse == 1 || joinResponse == 2 || joinResponse == 3)
                {
                    if(numOfUsers >= 2) 
                    {
                        readyUp.text = "All Players Ready!";
                        startTheGame.text = "Start the Game!";
                    }             
                    
                }
                else if (joinResponse == 9) //full
                {
                    print("Game is full");
                    //send us back to the first screen
                    GoToPanel(Panel.Connection);
                }
                else if (joinResponse == 4 || joinResponse == 5 || joinResponse == 6 || joinResponse == 7 || joinResponse == 8)//username is bad
                {
                    print("Username was bad, code: " + joinResponse);
                    //go back to the username screen
                    GoToPanel(Panel.Username);
                }

                //consume the data
                buffer.Consume(6);

                break;
            case "UPDT":

                if (buffer.Length < 48) return;

                byte whoseTurn = buffer.ReadByte(4);
                byte gameStatus = buffer.ReadByte(5);

                byte [] spaces = new byte[42];
                for(int i = 0; i < 42; i ++)
                {
                    spaces[i] = buffer.ReadByte(6+i);
                }
                print("game updated");

                //TODO switch to our gameplay screen
                GoToPanel(Panel.Gameplay);

                //TODO update the game
                game.UpdateFromServer(gameStatus, whoseTurn, spaces);

                buffer.Consume(48);

                break;
            case "CHAT":

                if (buffer.Length < 6) return;

                int usernameLength = buffer.ReadByte(4);
                int messageLength = buffer.ReadByte(5);

                int fullPacketLength = 7 + usernameLength + messageLength;

                if (buffer.Length < fullPacketLength) 
                {
                    print("blahhh");
                    return;
                }

                string username = buffer.ReadString(6, usernameLength);
                string message = buffer.ReadString(7 + usernameLength, messageLength);

                print (message);
                print(username);

                //TODO: switch to gameplay:
                GoToPanel(Panel.Gameplay);

                //TODO: update chat view

                buffer.Consume(fullPacketLength);

                AddMessageToChatDisplay(username, message);


                break;
            
            // case "STRT":

            //     if(buffer.Length < 5) return;

            //     int numberOfClients = buffer.ReadInt8(4);
            //     print(numberOfClients);
            //     print("We can start now");
            //     buffer.Consume(5);
                
            //     break;
            default:
                print("Uknown packet indentifier...");
                buffer.Clear();
                break;
        }
    }

    async public void SendPacketToServer(Buffer packet)
    {
        if(!socket.Connected) return;

        await socket.GetStream().WriteAsync(packet.Bytes, 0, packet.Bytes.Length);
    }

    public void SendPlayPacket(int x, int y)
    {
        SendPacketToServer(PacketBuilder.Play(x,y));

    }

    



}
