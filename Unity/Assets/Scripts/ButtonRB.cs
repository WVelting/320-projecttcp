using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using TMPro;

public struct GridPos
{
    public int X;
    public int Y;

    public GridPos(int X, int Y)
    {
        this.X = X;
        this.Y = Y;
        
    }

    public override string ToString()
    {
        return $"Grid position: ({X}, {Y})";
    }
}


public class ButtonRB : MonoBehaviour
{
    public Button button;
    public GridPos pos;
    public TextMeshProUGUI textField;
    
    
    public void Init(GridPos pos, UnityAction callback)
    {
        this.pos = pos;
        button.onClick.AddListener(callback);
    }

    public void SetOwner(byte b)
    {
        if(b == 0) textField.text = "";
        if(b == 1) textField.text = "RED";
        if(b == 2) textField.text = "BLUE";

    }


}
